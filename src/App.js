import React, { Component } from 'react';
import Main from './pages/Main'
import './App.css';
import './styles/main.css'

class App extends Component {
  render() {
    const appTitle = 'shyftplan';
    const appStage = 'test';

    return (
      <div className="App">
        <div className='row'>

        <div className='app-header'>
          <div className='app-title'>
              <span className='caption'>{appTitle}</span>
              <span className='stage'>{appStage}</span>
          </div>
        </div>
        <div className='clearfix'></div>
        <div className='app-content'>
          <div className='content'>
            <Main></Main>
          </div>
        </div>

        <div className='footer'></div>

        </div>

      </div>
    );
  }
}

export default App;

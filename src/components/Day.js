import React from 'react';
import '../styles/main.css';
import '../styles/bootstrap/dist/css/bootstrap.min.css'

const EventList = (props) => {
 
    return<ul className='event-todo-container'>

            {props.me.events.map((item, index)=>{
                const me = props.me;
                const day = props.me.day;
                const date = props.me.date;

                return (
                <li className='event-todo' onClick={()=>props.onItemClicked(item, me, day, date, index)} key={index}>
                    <div className='clearfix'></div>

                    <div className='event-todo-item'>
                        
                        <div className='view'>
                            <div className='duration'>
                                {item.startHour}:{item.startMinute} - {item.endHour}:{item.endMinute}
                            </div> 
                            <div className='clearfix'></div>
                            <div className='title'>{item.title}</div>
                        </div>
                        <div className='event-todo-action'>
                            <div className='delete' onClick={()=>props.onItemRemove(item, me, index)}>x</div>
                        </div>
                       
                    </div>

                </li>)
            })}
        </ul>
}

const Day = (props) =>{

    const containerStyle = {
        height: '100%'
    }
    return (
        <div className='col-md-12 col-lg-12 col-sm-12 col-xs-12' style={containerStyle} onClick={()=>props.eventClicked(props.me.day, props.me.date, props.me, props.data)}>
            <div className='date-header col-md-12 col-lg-12 col-sm-12 col-xs-12'>
                <div className='col-md-6 col-lg-6 col-sm-6 col-xs-6 weekday'>{props.me.day}</div>
                <div className='row'></div>
                <div className='col-md-6 col-lg-6 col-sm-6 col-xs-6 date'>{props.me.date}</div>
            </div>

            <div className='row'></div>

            <EventList onItemRemove={props.itemRemoved} onItemClicked={props.itemClicked} me={props.me} everyItem={props.data}></EventList>

        </div>
    )
}

export default Day;
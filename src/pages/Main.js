import React, { Component } from 'react';
import Day from '../components/Day';
import _ from 'lodash';
import '../styles/bootstrap/dist/css/bootstrap.min.css';

import dateFormat from 'dateformat';

class Main extends Component {

  state = {
    showOverlay: false,
    showWarning: false,
    endHour: '00',
    endMinute:'00',
    startHour: '00',
    startMinute: '00',
    title: '',
    date: '',
    day: ''
  }

  isUpdate = false;
  isRemove = false;
  index = 0;
  dateIndex = 0;
  initDateIndex = 0;
  item = {};
  list = [];
  startMinute='';
  endMinute = '';
  showDateBack = false;
  showDateForward = false;

  month = '';
  thisWeekDates = [];
  actionType = 'ADD';
  headerTitle = '';
  flag = true;


  itemHandler = (val, list, day, date, index)=>{
      

      if(!this.isRemove){
        this.dateIndex = _.findIndex(this.thisWeekDates, list);
        this.initDateIndex = this.dateIndex;
        this.showDateBack = true;
        this.showDateForward = true;
        this.headerTitle = 'Update'; 
        this.isUpdate = true;
        this.index = index;
        this.item = val;
        this.list = list;
        this.actionType = 'update';

        this.setState({
          showOverlay : true
        })
      
        this.initPopup(val, day, date);
    }
  }

  itemRemoveHandler = (item, list, index) => {
    this.headerTitle = 'Warning';

    this.isRemove = true;
    this.item = item;
    this.index = index;
    this.list = list;

    this.setState({
      showWarning: true
    })

  }

  initPopup = (val, day, date) => {
    this.setState({
      title : val.title,
      startHour: val.startHour,
      startMinute: val.startMinute,
      endHour: val.endHour,
      endMinute: val.endMinute,
      day: day,
      date: date
    })
  }

  getHours = (startfrom) =>{
    let hours = [];
    for(let i=startfrom;i<24;i++){
      if((i+'').length === 1){
        //prepend '0' to i
        hours.push('0'+i)
      }else{
        hours.push(i+'');
      }
    }

    return hours;
  }

  getMinutes = () => {
    let minutes = [];
    for(let i=0;i<60;i++){
      //prepend '0' to i
      if((i+'').length === 1){
        minutes.push('0'+i);
      }else{
        minutes.push(i+'');
      }
    }

    return minutes;
  }

  renderHours = (startHour) => {
    return this.getHours(parseInt(startHour)).map((hour, i)=>{
      return <option key={i} value={hour}>{hour}</option>
    })
  }

  renderMinutes = ()=>{
    return this.getMinutes().map((minute, i)=>{
      return <option key={i} value={minute}>{minute}</option>
    })
  }

  getWeekDates = () =>{
    
    let today = new Date();

    //Grab
    let fullDate = dateFormat(today, 'ddd,mmmm,dd');

    this.month = fullDate.split(',')[1];
    const weekDay = fullDate.split(',')[0].toLowerCase();
    let tmpBeginDate = 0;

    //Get start of week
    switch(weekDay){
      case 'tue':{
        tmpBeginDate = 1;
        break;
      }
      case 'wed':{
        tmpBeginDate = 2;
        break;
      }
      case 'thu':{
        tmpBeginDate = 3;
        break;
      }
      case 'fri':{
        tmpBeginDate = 4;
        break;
      }
      case 'sat':{
        tmpBeginDate = 5;
        break;
      }
      case 'sun':{
        tmpBeginDate = 6;
        break;
      }
      default:{
        tmpBeginDate = 0;
      }
    }

    let beginDate = new Date();


    beginDate.setDate(new Date().getDate() - tmpBeginDate);

    let thisWeek = [];
    for(let i=0; i<7; i++){
      let date = new Date(beginDate);

      date.setDate(date.getDate()+i);
      thisWeek.push(date);
    }   

    return thisWeek
  }

  onStartHourHandler = (event) => {
    this.setState({
      startHour: event.target.value
    })
  }

  onEndHourHandler = (event) => {
    this.setState({
      endHour: event.target.value
    })
  }

  onStartMinuteHandler = (event) => {
    this.setState({
      startMinute: event.target.value
    }) 
  }

  onEndMinuteHandler = (event) => {
    this.setState({
      endMinute: event.target.value
    }) 
  }

  onDoneHandler = () => {
    this.setState({
      showOverlay: false
    })

    let event = {startHour : this.state.startHour, 
                  endHour: this.state.endHour, 
                  startMinute: this.state.startMinute,
                  endMinute: this.state.endMinute,
                  title: this.state.title.toUpperCase()};
    
    if(this.isUpdate && (this.initDateIndex === this.dateIndex)){
      //reposition and order
      let ordered = _.orderBy(this.insertIntoPosition(this.index, event, this.list.events), ['startHour', 'title'],['asc', 'asc']);

      this.list.events = ordered;
    }else if(!this.isUpdate && (this.initDateIndex === this.dateIndex)){
      this.list.events.push(event);

      this.list.events = _.orderBy(this.list.events, ['startHour', 'title'], ['asc', 'asc']);
    }else if(this.initDateIndex !== this.dateIndex){
      //Find index
      
      //Grab date event and add
      let dateEvent = this.thisWeekDates[this.dateIndex];
      dateEvent.events.push(event);
      dateEvent.events = _.orderBy(dateEvent.events, ['startHour', 'title'], ['asc', 'asc']);
      this.thisWeekDates = this.insertIntoPosition(this.dateIndex, dateEvent, this.thisWeekDates);

      //Remove it from origin
      let originDateEvent =  this.thisWeekDates[this.initDateIndex];
      originDateEvent.events.splice(this.index, 1);
      originDateEvent.events = _.orderBy(originDateEvent.events, ['startHour', 'title'], ['asc', 'asc']);
      this.thisWeekDates = this.insertIntoPosition(this.initDateIndex, originDateEvent, this.thisWeekDates);
    }


   this.reset();
  }

  onRemoveHandler = () => {
    if(this.list){
      this.list.events.splice(this.index, 1);
    }

    this.setState({
      showOverlay: false
    })

    this.reset();
  }

  insertIntoPosition = (position, item, arr)=>{
    let head = arr.slice(0, position);
    let tail = arr.slice(position+1);

    head.push(item);

    return head.concat(tail);
  }

  onCloasHandler = () => {
    
    this.setState({
      showOverlay: false
    })

    this.reset();
  }

  onRemvoeCloasHandler = () => {
    this.setState({
      showWarning: false
    })

    this.reset();
  }

  onRemoveContinueHandler = () => {
    
    this.list.events.splice(this.index, 1);

    this.setState({
      showWarning: false
    })

    this.reset();
  }

  onRemoveCancelHandler =() => {
    this.setState({
      showWarning: false
    })

    this.reset();
  }

  onTitleChangeHandler = (event) =>{
    this.setState({
      title: event.target.value
    })
  }

  onEventClickedHandler = (day, date, list) => {

    this.dateIndex = _.findIndex(this.thisWeekDates, list);

    if(!this.headerTitle.length > 0){
      this.headerTitle = 'Add Event';
      this.actionType = 'add';
      this.list = list;
      this.showDateBack = false;
      this.showDateForward = false;
      this.isUpdate = false;
      this.initDateIndex = this.dateIndex;


      this.setState({
        showOverlay: true,
        date : date,
        day: day
      })
    }

    // this.setState({
    //   showOverlay: true,
    //   date : date,
    //   day: day
    // })
  }

  onDateForwardHandler = () => {
    if(!this.isUpdate){
      return;
    }

    if((this.dateIndex+1) <= ((this.thisWeekDates.length)-1)){
      //Move date
      this.dateIndex ++;
      const newDate = this.thisWeekDates[this.dateIndex];

      this.setState({
        day : newDate.day,
        date: newDate.date
      })

    }
  }

  onDateBackHandler = () => {
    if(!this.isUpdate){
      return;
    }

    if((this.dateIndex-1) >= 0){
      //Move date
      this.dateIndex --;
      const newDate = this.thisWeekDates[this.dateIndex];

      this.setState({
        day : newDate.day,
        date: newDate.date
      })

    }
  }

  reset = () =>{
    this.headerTitle='';
    this.isUpdate = false;
    this.index = 0;
    this.item = {};
    this.list = [];
    this.actionType = '';
    this.dateIndex = 0;
    this.initDateIndex = 0;
    this.isRemove = false;

    this.setState({
      date: '',
      day: '',
      title: '',
      startHour: '00',
      startMinute: '00',
      endHour: '00',
      endMinute: '00'
    })
  }

  renderOverlay = ()=>{
    const vamus = {
      visibility: 'hidden'
    }

    if(this.state.showOverlay){

    return (
        <div className={this.state.showOverlay ? 'overlay':''}>
          <div className={this.state.showOverlay ? 'dialog':'hide'}>
            <div className='header'>
              <div className='header-title'>{this.headerTitle}</div>
              <div className='d-close' onClick={this.onCloasHandler}>x</div>
            </div>
            <div className='clearfix'></div>
            <div className='content'>

              <div className='date-container'>
                <div className='d-day'>{this.state.day}</div>
                <div className='clearfix'></div>
                <div className='d-date-container'>
                  <div className='d-back' style={!this.showDateBack?vamus:{}} onClick={this.onDateBackHandler}>{'<'}</div>
                  <div className='d-date'>{this.state.date}</div>
                  <div className='d-forward' style={!this.showDateBack?vamus:{}} onClick={this.onDateForwardHandler}>{'>'}</div>
                </div>
              </div>

              <div className='clearfix'></div>

              <div className='title-container'>
                <input type='text' placeholder='Add Title' value={this.state.title} onChange={this.onTitleChangeHandler}></input>
              </div>

              <div className='clearfix'></div>

              <div className='time-container'>

                <div className='time-icon'>
                  <i className="fa fa-time" aria-hidden="true"></i>
                </div>

                <div className='date-time-container'>
                  <select type='text' value={this.state.startHour} className='s-hour' onChange={this.onStartHourHandler}>
                    {this.renderHours(0)}
                  </select>

                  <div className='s-separator'>:</div>
                  <select type='text' value={this.state.startMinute} className='s-minutes' onChange={this.onStartMinuteHandler}>
                    {this.renderMinutes()}
                  </select>

                  <div className='dash'>-</div>

                  <select type='text' value={this.state.endHour} className='e-hour' onChange={this.onEndHourHandler}>
                    {this.renderHours(this.state.startHour)}
                  </select>
                  <div className='e-separator'>:</div>
                  <select type='text' value={this.state.endMinute} className='e-minutes' onChange={this.onEndMinuteHandler}>
                    {this.renderMinutes()}
                  </select>
                </div>

                <div className='clearfix'></div>

                <div className='actions'>
                  <button className='update-button' onClick={this.onDoneHandler} >{this.actionType}</button>
                  {this.isUpdate? <div className='d-remove' onClick={this.onRemoveHandler} >remove</div>:''}
                </div>

              </div>
            </div>
          </div>
        </div>
    )
  }else if(this.state.showWarning){
    const overlayHeader = 'Warninig';
    const question = 'Are you sure to remove this event?'

    return (
      <div className={this.state.showWarning ? 'overlay':''}>
          <div className={this.state.showWarning ? 'warning-dialog':'hide'}>
            <div className='header'>
              <div className='header-title'>{overlayHeader}</div>
              <div className='d-close' onClick={this.onRemvoeCloasHandler}>x</div>
            </div>
            <div className='clearfix'></div>

            <div className='content'>
              <div className='time-container'>
                <div className='warning-message'>{question}</div>
                <div className='clearfix'></div>
                <div className='actions'>
                  <button className='update-button' onClick={this.onRemoveContinueHandler} >Continue</button>
                  <div className='d-remove' onClick={this.onRemoveCancelHandler} >cancel</div>
                </div>

              </div>
            </div>
          </div>
        </div>
    )
  }
  } 

  renderDays=()=>{

    if(this.flag){

      this.thisWeekDates = this.getWeekDates();
      this.thisWeekDates = this.thisWeekDates.map((date)=>{
        let fineDate = dateFormat(date, 'ddd,mmmm,dd');
        let tmpdate = parseInt(fineDate.split(',')[2]);
        let day = fineDate.split(',')[0];
        return {date: tmpdate, 
                day, 
                // events:[{title:'i will dance', startHour:'02', startMinute:'22', endHour: '03', endMinute:'00'},
                //         {title:'i will dance', startHour:'06', startMinute:'23', endHour: '05', endMinute:'23'},
                //         {title:'i will dance', startHour:'09', startMinute:'01', endHour: '10', endMinute:'12'},
                //         {title:'i will dance', startHour:'12', startMinute:'22', endHour: '13', endMinute:'22'},
                //         {title:'i will dance', startHour:'02', startMinute:'01', endHour: '03', endMinute:'00'},
                //         {title:'i will dance', startHour:'20', startMinute:'00', endHour: '22', endMinute:'03'},
                //       ]
                events: []
              }
      })

      this.flag = false;
    }

    return this.thisWeekDates.map((day, i)=>{
      const data = this.thisWeekDates;

      return <div key={i} className='day'>
                <Day key={i} data={data} me={day} itemRemoved={this.itemRemoveHandler} itemClicked={this.itemHandler} eventClicked={this.onEventClickedHandler}></Day>
            </div>
    })
  }

  render() {

    return (
      <div className='col-md-12 col-lg-12'>

        {this.renderDays()}
        {this.renderOverlay()}
      </div>
    );
  }
}

export default Main;
